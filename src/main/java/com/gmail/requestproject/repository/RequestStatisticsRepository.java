package com.gmail.requestproject.repository;

import com.gmail.requestproject.domain.Request;
import com.gmail.requestproject.domain.RequestStatistics;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestStatisticsRepository extends JpaRepository<RequestStatistics, Long> {

    RequestStatistics findById(Integer id);

}
