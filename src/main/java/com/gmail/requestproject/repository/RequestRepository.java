package com.gmail.requestproject.repository;

import com.gmail.requestproject.domain.Request;
import com.gmail.requestproject.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface RequestRepository extends JpaRepository<Request, Long> {

    Request findById(Integer id);

    ArrayList<Request> findByUser(User user);

    Page<Request> findAll(Pageable pageable);

    Page<Request> findByUser(Pageable pageable, User user);
}
