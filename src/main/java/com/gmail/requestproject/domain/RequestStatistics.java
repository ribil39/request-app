package com.gmail.requestproject.domain;

import javax.persistence.*;

@Entity
@Table(name = "T_STATISTICS")
public class RequestStatistics {

    @Id
    @Column(name = "F_ID")
    private int id;

    @Column(name = "F_ACCEPTED")
    private int accepted;

    @Column(name = "F_DECLINED")
    private int declined;

    public RequestStatistics() {
    }


    public RequestStatistics(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAccepted() {
        return accepted;
    }

    public void setAccepted(int accepted) {
        this.accepted = accepted;
    }

    public int getDeclined() {
        return declined;
    }

    public void setDeclined(int declined) {
        this.declined = declined;
    }
}
