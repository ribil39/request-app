package com.gmail.requestproject.domain;

public enum Status {
    NEW, DONE, DECLINED
}
