package com.gmail.requestproject.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "T_REQUEST")
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "F_ID")
    private int id;

    @Column(name = "F_REQUEST")
    private String request;

    @Column(name = "F_BID")
    private int bid;

    @Column(name = "F_DUE_DATE")
    private Date dueDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "F_USER_ID")
    private User user;

    @Enumerated(EnumType.STRING)
    @Column(name = "F_STATUS")
    private Status status;

    public Request() {
    }

    public Request(String request, int bid, Date dueDate, User user) {
        this.request = request;
        this.bid = bid;
        this.dueDate = dueDate;
        this.user = user;
    }

    public String getUserName() {
        return user.getUsername();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request1 = (Request) o;
        return id == request1.id &&
                bid == request1.bid &&
                Objects.equals(request, request1.request) &&
                Objects.equals(dueDate, request1.dueDate) &&
                Objects.equals(user, request1.user) &&
                status == request1.status;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, request, bid, dueDate, user, status);
    }
}
