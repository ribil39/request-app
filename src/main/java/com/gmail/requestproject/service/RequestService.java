package com.gmail.requestproject.service;

import com.gmail.requestproject.domain.Request;
import com.gmail.requestproject.domain.User;
import com.gmail.requestproject.repository.RequestRepository;
import com.gmail.requestproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestService {

    @Autowired
    RequestRepository requestRepository;

    @Autowired
    UserRepository userRepository;

    public Page<Request> findAllRequestsPage(Pageable pageable) {

        Page<Request> requestPage = requestRepository.findAll(pageable);

        return requestPage;
    }

    public Page<Request> findRequestsByUser(Pageable pageable, User user) {

        Page<Request> requestPage = requestRepository.findByUser(pageable, user);

        return requestPage;
    }

    public List<Request> findAllRequests() {

        List<Request> requests = requestRepository.findAll();

        return requests;

    }

    public Request findRequestById(Integer id) {

        Request request = requestRepository.findById(id);

        return request;
    }

    public void saveRequest(Request request) {
        requestRepository.save(request);
    }

    public List<Request> findRequestsByAuthor(Integer id) {

        List<Request> requests = requestRepository.findByUser(userRepository.findById(id));

        return requests;
    }
}
