package com.gmail.requestproject.service;

import com.gmail.requestproject.domain.RequestStatistics;
import com.gmail.requestproject.repository.RequestStatisticsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RequestStatisticsService {

    @Autowired
    RequestStatisticsRepository requestStatisticsRepository;

    public RequestStatistics findStatisticsById(Integer id){
        RequestStatistics statistics = requestStatisticsRepository.findById(id);
        return statistics;
    }

    public void saveStatistics(RequestStatistics statistics){
        requestStatisticsRepository.save(statistics);
    }

    public void increaseAccepted(){

         RequestStatistics statistics = requestStatisticsRepository.findById(1);
         statistics.setAccepted(statistics.getAccepted() + 1);
         requestStatisticsRepository.save(statistics);
    }

    public void increaseDeclined(){

        RequestStatistics statistics = requestStatisticsRepository.findById(1);
        statistics.setDeclined(statistics.getDeclined() + 1);
        requestStatisticsRepository.save(statistics);
    }

    public RequestStatistics getStatistics() {
        if(requestStatisticsRepository.findById(1) == null){
            RequestStatistics stat = new RequestStatistics(1);
            requestStatisticsRepository.save(stat);
        }
        RequestStatistics statistics = requestStatisticsRepository.findById(1);
        return statistics;
    }
}
