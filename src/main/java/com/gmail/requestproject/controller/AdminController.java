package com.gmail.requestproject.controller;

import com.gmail.requestproject.domain.Request;
import com.gmail.requestproject.domain.RequestStatistics;
import com.gmail.requestproject.domain.Status;
import com.gmail.requestproject.domain.User;
import com.gmail.requestproject.service.RequestService;
import com.gmail.requestproject.service.RequestStatisticsService;
import com.gmail.requestproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
@PreAuthorize("hasAuthority('ADMIN')")
public class AdminController {

    @Autowired
    RequestService requestService;

    @Autowired
    UserService userService;

    @Autowired
    RequestStatisticsService requestStatisticsService;

    @GetMapping("/admin")
    public String mainAdmin(
            Map<String, Object> model,
            @AuthenticationPrincipal User user,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable
    ) {

        Page<Request> requestPage = requestService.findAllRequestsPage(pageable);

        model.put("page", requestPage);
        model.put("url", "/");

        model.put("user", user);

        RequestStatistics statistics = requestStatisticsService.getStatistics();

        model.put("statistics", statistics);

        return "admin";
    }

    @PostMapping("/perform")
    public String statusDone(
            @RequestParam int id
    ) {
        Request request = requestService.findRequestById(id);

        if (!request.getStatus().equals(Status.DONE)) {
            requestStatisticsService.increaseAccepted();
        }

        request.setStatus(Status.DONE);


        requestService.saveRequest(request);

        return "redirect:admin/";


    }

    @PostMapping("/decline")
    public String statusDeclined(
            @RequestParam int id
    ) {
        Request request = requestService.findRequestById(id);

        if (!request.getStatus().equals(Status.DECLINED)) {
            requestStatisticsService.increaseDeclined();
        }

        request.setStatus(Status.DECLINED);


        requestService.saveRequest(request);


        return "redirect:admin/";


    }
}
