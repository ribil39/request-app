package com.gmail.requestproject.controller;


import com.gmail.requestproject.domain.Request;
import com.gmail.requestproject.domain.Status;
import com.gmail.requestproject.domain.User;
import com.gmail.requestproject.service.RequestService;
import com.gmail.requestproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Controller
public class UserController {

    @Autowired
    RequestService requestService;

    @Autowired
    UserService userService;

    @GetMapping("/")
    public String main(
            Map<String, Object> model,
            @AuthenticationPrincipal User user,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable
    ) {
        if (user.isAdmin()) {
            return "redirect:admin/";
        }

        Page<Request> requestPage = requestService.findRequestsByUser(pageable, user);

        model.put("page", requestPage);
        model.put("url", "/");

        model.put("user", user);

        return "main";
    }

    @PostMapping("/addrequest")
    public String add(@AuthenticationPrincipal User user,
                      @RequestParam String requestText,
                      @RequestParam int requestBid,
                      @RequestParam String requestDueDate
                      ) throws ParseException {

        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(requestDueDate);
        Request request = new Request(requestText, requestBid, date, user);
        request.setStatus(Status.NEW);
        requestService.saveRequest(request);
        return "redirect:/";
    }

}
