<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>

<@c.page>
<div class="container">

    <h5 class="mt-4">Default user:</h5>
    <h6>login - user1</h6>
    <h6>password - 123</h6>
    <h5>Default admin:</h5>
    <h6>login - admin</h6>
    <h6>password - 123</h6>
    <br>

<p>${message?ifExists}</p>
<@l.login "/login" false/>
</div>
</@c.page>
