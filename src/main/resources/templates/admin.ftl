<#import "parts/common.ftl" as c>
<#import "parts/addform.ftl" as a>
<#import "parts/pager.ftl" as p>
<#include "parts/security.ftl">

<@c.page>

<div class="container">
    <div class="row">
        <div class="col-sm-8 col-md-8 mt-5">

            <h4 class="my-5">ADMIN PAGE</h4>

            <h5>Total done: ${statistics.getAccepted()}</h5>
            <h5>Total declined: ${statistics.getDeclined()}</h5>

            <h5 class="mt-5">All requests</h5>

    <@p.pager url page />
    <#list page.content as request>
                    <div class="card border-light mb-3" style="max-width: 22rem, border-color: gray;">
                        <div class="card-header">${request.getUserName()}</div>
                        <div class="card-body">
                            <p class="card-text">Request: ${request.getRequest()}</p>
                            <p class="card-text">Bid: ${request.getBid()}</p>
                            <p class="card-text">Due date: ${request.getDueDate()?date}</p>
                            <p class="card-text">
                            <h5><span class="badge badge-dark">
                                ${request.getStatus()}
                            </span></h5>
                            </p>


                            <form method="post" class="float-left mt-3" action="/perform">
                                <input type="hidden" class="form-control" name="id" value="${request.getId()}"
                                       aria-describedby="basic-addon2">
                                <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                                <div class="input-group-append">
                                    <button class="btn btn-success" type="submit">Perform</button>
                                </div>
                            </form>

                <form method="post" class="float-left mt-3 ml-4" action="/decline">
                    <input type="hidden" class="form-control" name="id" value="${request.getId()}"
                           aria-describedby="basic-addon2">
                    <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                    <div class="input-group-append">
                        <button class="btn btn-danger" type="submit">Decline</button>
                    </div>
                            </form>


                        </div>
                    </div>

    <#else>
                    <h5>No requests =(</h5>
    </#list>

        </div><!--End col-sm-md-->
    </div><!--End row-->
</div><#--End container-->

</@c.page>