<#include "security.ftl">
<#import "login.ftl" as l>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/">Request App</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">

    </div>
    <div class="navbar-text mr-2">${name}</div>
    <#if !isUser>
        <form class="form-inline" action="/login">
            <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Log in</button>
        </form>
    <#else>
        <@l.logout />
    </#if>
</nav>
