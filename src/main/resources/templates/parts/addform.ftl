<#macro addform path>

<form method="post" action="/addrequest">
    <div class="form-row">
        <div class="col-md-6 mb-3">

            <input type="text" class="form-control mb-3" name="requestText" placeholder="Request"
                   aria-describedby="basic-addon2">
            <input type="text" class="form-control mb-3" name="requestBid" placeholder="Bid"
                   aria-describedby="basic-addon2">
            <input type="date" class="mb-3" id="end" name="requestDueDate"
                   value="2018-10-22"
                   min="2018-01-01" max="2030-12-31"/>

            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <div class="input-group-append">
                <button class="btn btn-dark" type="submit">Add</button>
            </div>
        </div>
    </div>
</form>
</#macro>