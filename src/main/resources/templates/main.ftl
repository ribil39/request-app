<#import "parts/common.ftl" as c>
<#import "parts/addform.ftl" as a>
<#import "parts/pager.ftl" as p>
<#include "parts/security.ftl">

<@c.page>

<div class="container">
    <div class="row">
        <div class="col-sm-8 col-md-6 mt-5">

            <h4 class="my-5">Please, add your request</h4>

            ${warning?ifExists}
        <@a.addform "/add" />

            <h5 class="my-5">My requests:</h5>

 <@p.pager url page />
                <#list page.content as request>
                    <div class="card border-light mb-3" style="max-width: 22rem, border-color: gray;">
                        <div class="card-header">${request.getUserName()}</div>
                        <div class="card-body">
                            <p class="card-text">Request: ${request.getRequest()}</p>
                            <p class="card-text">Bid: ${request.getBid()}</p>
                            <p class="card-text">Due date: ${request.getDueDate()?date}</p>
                            <p class="card-text">
                            <h4><span class="badge badge-primary">
                                ${request.getStatus()}
                            </span></h4>
                            </p>
                        </div>
                    </div>

                <#else>
                    <h5>No requests =(</h5>
                </#list>

        </div><!--End col-sm-md-->
    </div><!--End row-->
</div><#--End container-->

</@c.page>